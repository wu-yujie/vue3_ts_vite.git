import { presetUno, presetAttributify } from 'unocss';
import transformerDirective from '@unocss/transformer-directives';
import { defineConfig } from 'vite';
export default defineConfig({
  presets: [presetUno(), presetAttributify()],
  transformers: [transformerDirective()],
  theme: {
    // textColor: (theme) => theme('colors'),
    colors: {
      primary: '#377DFF',
      success: '#1ac468',
      warning: '#ffac07',
      error: '#ff3b3d',
      danger: '#FD5E3A',
      c_90: '#909399',
      c_52: '#525966',
      c_9d: '#9DA3B2',
      c_81: '#818999',
      c_d8: '#d8E0F5',
      c_2d: '#2D2F33',
      c_fd: '#FDEED3',
      c_F5: '#F5F7FA',
      c_F2: '#F2F3F7',
      c_dd: '#DDE1EB',
      c_1b: '#1B1B1B',
    },
    backgroundColor: (theme) => ({
      ...theme('colors'),
    }),
    borderColor: (theme) => ({
      ...theme('colors'),
    }),
    fontSize: {
      // 12: '12px',
    },
  },
  rules: [
    // 方式1
    [
      'p-a',
      {
        position: 'absolute',
      },
    ],
    // 方式2
    [/^text-(\d+)$/, ([, d]) => ({ 'font-size': `${d}px` })],
    [/^h-(\d+)$/, ([, d]) => ({ height: `${d}px` })],
    [/^w-(\d+)$/, ([, d]) => ({ width: `${d}px` })],
    [/^min-h-(\d+)$/, ([, d]) => ({ 'min-height': `${d}px` })],
    [/^min-w-(\d+)$/, ([, d]) => ({ 'min-width': `${d}px` })],
    [/^left-(\d+)$/, ([, d]) => ({ left: `${d}px` })],
    [/^top-(\d+)$/, ([, d]) => ({ top: `${d}px` })],
    [/^right-(\d+)$/, ([, d]) => ({ right: `${d}px` })],
    [/^bottom-(\d+)$/, ([, d]) => ({ bottom: `${d}px` })],
    [/^m-(\d+)$/, ([, d]) => ({ margin: `${d}px` })],
    [/^ml-(\d+)$/, ([, d]) => ({ 'margin-left': `${d}px` })],
    [/^mt-(\d+)$/, ([, d]) => ({ 'margin-top': `${d}px` })],
    [/^mr-(\d+)$/, ([, d]) => ({ 'margin-right': `${d}px` })],
    [/^mb-(\d+)$/, ([, d]) => ({ 'margin-bottom': `${d}px` })],
    [/^p-(\d+)$/, ([, d]) => ({ padding: `${d}px` })],
    [/^pl-(\d+)$/, ([, d]) => ({ 'padding-left': `${d}px` })],
    [/^pt-(\d+)$/, ([, d]) => ({ 'padding-top': `${d}px` })],
    [/^pr-(\d+)$/, ([, d]) => ({ 'padding-right': `${d}px` })],
    [/^pb-(\d+)$/, ([, d]) => ({ 'padding-bottom': `${d}px` })],
    [/^px-(\d+)$/, ([, d]) => ({ 'padding-left': `${d}px`, 'padding-right': `${d}px` })],
    [/^py-(\d+)$/, ([, d]) => ({ 'padding-top': `${d}px`, 'padding-bottom': `${d}px` })],
    [/^mx-(\d+)$/, ([, d]) => ({ 'margin-left': `${d}px`, 'margin-right': `${d}px` })],
    [/^my-(\d+)$/, ([, d]) => ({ 'margin-top': `${d}px`, 'margin-bottom': `${d}px` })],
  ],
});
